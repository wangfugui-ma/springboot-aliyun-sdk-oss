package com.masiyi.aliyun;

import com.masiyi.aliyun.service.AliyunService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AliyunApplicationTests {

    @Autowired
    private AliyunService aliyunService;

    @Test
    void contextLoads() {
        aliyunService.putObject();
    }

}
