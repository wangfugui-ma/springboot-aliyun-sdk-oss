package com.masiyi.aliyun;


import com.aliyun.oss.OSS;
import com.masiyi.aliyun.service.AliyunService;
import com.masiyi.aliyun.util.AliyunUtil;
import lombok.extern.java.Log;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringBootTest
class ApprenticeApplicationTests {


    @Autowired
    private AliyunService aliyunService;
    @Autowired
    private AliyunUtil aliyunUtil;


    private static String filePath; // 选择的markdown文件的路径


    /**
     * 生成代码
     *
     * @Param: []
     * @return: void
     * @Author: MaSiyi
     * @Date: 2021/11/13
     */
    @Test
    void generateCode() {

        filePath = "D:\\Backup\\Documents\\【Mybatis】我抄袭了Mybatis，手写一套MyMybatis框架：学会使用mybatis框架.md";
        String wordname = filePath.substring(filePath.lastIndexOf("\\") + 1, filePath.lastIndexOf("\\") + 9);

        // 构建副本路径和图片保存路径
        String imgPath = filePath.substring(filePath.lastIndexOf("\\") + 1, filePath.length() - 3) + "img";
        String destPath = new File("").getAbsolutePath() + "\\" + imgPath;

        try {
            // 将markdown文件读入内存，保存到字符串变量stringBuilder中
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null)
                stringBuilder.append(line).append("\n");
            br.close();

            // 获得图片网络地址，并从网络上下载图片到本地；最后把markdown文档的图片标签换成本地路径
            String mdContent = stringBuilder.toString();
            Pattern pattern = Pattern.compile("!\\[.*?]\\((https.*?)\\)"); // 匹配表达式
            Matcher matcher = pattern.matcher(mdContent);    // 构建匹配器
            File dir = new File(destPath);
            if (!dir.exists())
                dir.mkdirs(); // 创建图片存储位置的文件夹
            while (matcher.find()) {
                String imgUrl = matcher.group(1);   // 匹配匹配器第一个括号里的内容
                int index = imgUrl.indexOf('?');
                imgUrl = index == -1 ? imgUrl : imgUrl.substring(0, index);
                InputStream inputStream = new URL(imgUrl).openStream();
                String imgName = imgUrl.substring(imgUrl.lastIndexOf("/")); // 图片名称
                InputStream inputStream1 = aliyunUtil.doWithPhoto(inputStream);
                aliyunService.putImage(inputStream1, wordname + imgName);

                mdContent = mdContent.replaceFirst(imgUrl, "https://masiyimarkdown.oss-cn-guangzhou.aliyuncs.com/" + wordname + imgName);
            }
            // 构建副本
            FileWriter writer = new FileWriter(destPath.substring(0, destPath.length() - 3) + ".md");
            String str = mdContent.replace("在这里插入图片描述", "").replace("请添加图片描述", "") + "![](https://masiyimarkdown.oss-cn-guangzhou.aliyuncs.com/%E6%B1%82%E8%B5%9E2.png)";
            str = str.replace("@[toc]", "");
            str = str + "\n" +
                    "另外如果对Elastic Search感兴趣的话，推荐一下我的专栏，这篇专栏介绍了Elasticsearch的Restful API的入门指南。学习如何使用API进行索引、搜索和分析，包括创建索引、定义映射、添加文档、执行查询等。通过实例和代码片段，快速上手Elasticsearch的Restful API，构建强大的搜索功能。感谢大家支持：\n" +
                    "\n" +
                    "[Elastic Search的RestFul API入门\n" +
                    "](https://blog.csdn.net/csdnerm/category_12497418.html)\n" +
                    "\n" +
                    "> ![](https://masiyimarkdown.oss-cn-guangzhou.aliyuncs.com/%E4%BA%92%E8%81%94%E7%BD%91%E5%88%9B%E6%84%8F%E6%B5%85%E8%89%B2%E7%B3%BB%E4%B8%BB%E8%A7%86%E8%A7%89kv.jpg)\n";
            writer.write(str);
            writer.flush();
            writer.close();

//            JOptionPane.showMessageDialog(null, "任务完成！");
//            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
//            JOptionPane.showMessageDialog(null, "任务失败！", "IO异常", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
