package com.masiyi.aliyun.service;

import com.masiyi.aliyun.util.AliyunUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/**
 * @Author masiyi
 * @Date 2024/3/15
 * @PackageName:com.masiyi.aliyun.service
 * @ClassName: AliyunService
 * @Description: TODO
 * @Version 1.0
 */
@Service
public class AliyunService {

    @Autowired
    private AliyunUtil aliyunUtil;

    public void putObject() {
        aliyunUtil.putObject("你好", "objectName.txt");
    }

    public void putImage(InputStream inputStream, String imgName) {
        aliyunUtil.putImage(inputStream, imgName);
    }
}
